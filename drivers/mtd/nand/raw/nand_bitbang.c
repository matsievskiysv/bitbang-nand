// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (c) 2020, Cortina Access Inc..
 */

#include <linux/delay.h>
// #include <linux/bitops.h>
// #include <linux/sizes.h>
// #include <log.h>
// #include <asm/io.h>
// #include <memalign.h>
#include <linux/mtd/rawnand.h>
// #include <nand.h>
// #include <dm/device_compat.h>
// #include <linux/bug.h>
// #include <linux/delay.h>
// #include <linux/iopoll.h>
// #include <linux/errno.h>
// #include <linux/mtd/rawnand.h>
// #include <fdtdec.h>
// #include <bouncebuf.h>
#include <asm/global_data.h>
#include <asm/gpio.h>
#include <dm.h>
#include <dm/util.h> //remove

// #include "nand_MT29F_lld.h"

// static unsigned int *pread, *pwrite;

#define GPIO_SET_DELAY 5

struct gpio_fdt_info {
	int gpio_delay;
	int pin_ale;
	int pin_nce;
	int pin_cle;
	int pin_nre;
	int pin_nwe;
	int pin_nwp;
	int pin_rdy;
	int data_width;
	int pins_data[16];
};

struct nand_chips {

};

struct nand_driver_info {
	char chip_count;
	struct nand_chip chips[];
};

// static struct nand_ecclayout eccoob;
struct gpios {
	struct gpio_desc ale;
	struct gpio_desc nce;
	struct gpio_desc cle;
	struct gpio_desc nre;
	struct gpio_desc nwe;
	struct gpio_desc nwp;
	struct gpio_desc rdy;
	struct gpio_desc data[16]; // avoid dynamic allocation at cost of 8*sizeof(struct gpio_desc)
};

struct bb_nand_info {
	struct gpios gpios;
};

struct bb_nand_instance {
	unsigned int data_sz;
	struct gpios gpios;
};

// static uint8_t
// hw_read_byte(struct mtd_info *mtd)
// {
// 	struct bb_nand_info *info  = dev_get_priv(mtd->dev);
// 	struct gpios	     gpios = info->gpios;
// 	uint8_t		     data  = 0;

// 	for (int i = 0; i < NAND_DATA_WIDTH; i++) {
// 		dm_gpio_set_dir_flags(&gpios.data[i], GPIOD_IS_IN);
// 	}
// 	dm_gpio_set_value(&gpios.nwe, 1);
// 	dm_gpio_set_value(&gpios.nre, 1);
// 	udelay(GPIO_SET_DELAY);
// 	// trigger read
// 	dm_gpio_set_value(&gpios.nre, 0);
// 	udelay(GPIO_SET_DELAY);
// 	for (int i = 0; i < NAND_DATA_WIDTH; i++) {
// 		data |= (dm_gpio_get_value(&gpios.data[i]) << i);
// 	}
// #ifdef DEBUG_BITBANG
// 	printf("NAND Read: %c\n", data);
// #endif
// 	return data;
// }

// static void
// hw_write_byte(struct mtd_info *mtd, uint8_t byte)
// {
// 	struct bb_nand_info *info  = dev_get_priv(mtd->dev);
// 	struct gpios	     gpios = info->gpios;

// #ifdef DEBUG_BITBANG
// 	printf("NAND Write: %c\n", byte);
// #endif
// 	for (int i = 0; i < 8; i++) {
// 		dm_gpio_set_dir_flags(&gpios.data[i], GPIOD_IS_OUT);
// 		dm_gpio_set_value(&gpios.data[i], !!(byte & (1 << i)));
// 	}
// 	dm_gpio_set_value(&gpios.nre, 1);
// 	dm_gpio_set_value(&gpios.nwe, 0);
// 	udelay(GPIO_SET_DELAY);
// 	// trigger write
// 	dm_gpio_set_value(&gpios.nwe, 1);
// 	udelay(GPIO_SET_DELAY);
// }

// static void
// nand_hwcontrol(struct mtd_info *mtd, int cmd, unsigned int ctrl)
// {
// 	struct bb_nand_info *info  = dev_get_priv(mtd->dev);
// 	struct gpios	     gpios = info->gpios;

// #ifdef DEBUG_BITBANG
// 	printf("NAND CMD: %u:%u\n", cmd, ctrl);
// #endif

// 	if (ctrl & NAND_CTRL_CHANGE) {
// 		/* Select the chip by setting nCE to low */
// 		dm_gpio_set_value(&gpios.nce, !(ctrl & NAND_NCE));
// 		/* Select the command latch by setting CLE to high */
// 		dm_gpio_set_value(&gpios.cle, !!(ctrl & NAND_NCE));
// 		/* Select the address latch by setting ALE to high */
// 		dm_gpio_set_value(&gpios.ale, !!(ctrl & NAND_ALE));
// 	}

// 	if (!(cmd & NAND_CMD_NONE)) {
// 		for (int i = 0; i < 8; i++) {
// 			dm_gpio_set_dir_flags(&gpios.data[i], GPIOD_IS_OUT);
// 			dm_gpio_set_value(&gpios.data[i], !!(cmd & (1 << i)));
// 		}
// 		dm_gpio_set_value(&gpios.nre, 1);
// 		dm_gpio_set_value(&gpios.nwe, 0);
// 		udelay(GPIO_SET_DELAY);
// 		// trigger write
// 		dm_gpio_set_value(&gpios.nwe, 1);
// 		udelay(GPIO_SET_DELAY);
// 	}
// }

// void
// bitbang_gpio_free(struct udevice *dev, struct gpios *gpios)
// {
// 	dm_gpio_free(dev, &gpios->ale);
// 	dm_gpio_free(dev, &gpios->nce);
// 	dm_gpio_free(dev, &gpios->cle);
// 	dm_gpio_free(dev, &gpios->nre);
// 	dm_gpio_free(dev, &gpios->nwe);
// 	dm_gpio_free(dev, &gpios->nwp);
// 	dm_gpio_free(dev, &gpios->rdy);
// 	for (int i = 0; i < NAND_DATA_WIDTH; i++) {
// 		dm_gpio_free(dev, &gpios->data[i]);
// 	}
// }

// static int
// dts_decode_request(struct udevice *dev, struct bb_nand_instance *info)
// {

// #define get_gpio(devp, gpio_name, gpio_index, gpio_desc, flag, label)                                                  \
// 	do {                                                                                                           \
// 		if (gpio_request_by_name(devp, gpio_name, gpio_index, &gpio_desc, flag)) {                             \
// 			pr_err("pin %s #%d not found\n", gpio_name, gpio_index);                                       \
// 			goto label;                                                                                    \
// 		}                                                                                                      \
// 	} while (0)

// 	get_gpio(dev, "ale-gpios", 0, gpios->ale, GPIOD_IS_OUT, err_pin);
// 	get_gpio(dev, "cle-gpios", 0, gpios->cle, GPIOD_IS_OUT, err_pin);
// 	get_gpio(dev, "nre-gpios", 0, gpios->nre, GPIOD_IS_OUT, err_pin);
// 	get_gpio(dev, "nwe-gpios", 0, gpios->nwe, GPIOD_IS_OUT, err_pin);
// 	// NWP, NCS and RDY are not requred
// 	for (int i = 0; i < NAND_DATA_WIDTH; i++) {
// 		get_gpio(dev, "data-gpios", i, gpios->data[i], GPIOD_IS_IN, err_pin);
// 	}
// #undef get_gpio

// 	gpio_request_by_name(dev, "nwp-gpios", 0, &gpios->nwp, GPIOD_IS_OUT);
// 	gpio_request_by_name(dev, "nce-gpios", 0, &gpios->nce, GPIOD_IS_OUT);
// 	gpio_request_by_name(dev, "rdy-gpios", 0, &gpios->rdy, GPIOD_IS_IN);

// 	return 0;
// err_pin:
// 	bitbang_gpio_free(dev, gpios);
// 	return 1;
// }

static int
dts_decode_chip(ofnode node, struct gpio_fdt_info *info)
{

#define get_gpio(devp, gpio_name, gpio_index, gpio_desc, flag, label)                                                  \
	do {                                                                                                           \
		if (gpio_request_by_name(devp, gpio_name, gpio_index, &gpio_desc, flag)) {                             \
			pr_err("pin %s #%d not found\n", gpio_name, gpio_index);                                       \
			goto label;                                                                                    \
		}                                                                                                      \
	} while (0)

	get_gpio(dev, "ale-gpios", 0, gpios->ale, GPIOD_IS_OUT, err_pin);
	get_gpio(dev, "cle-gpios", 0, gpios->cle, GPIOD_IS_OUT, err_pin);
	get_gpio(dev, "nre-gpios", 0, gpios->nre, GPIOD_IS_OUT, err_pin);
	get_gpio(dev, "nwe-gpios", 0, gpios->nwe, GPIOD_IS_OUT, err_pin);
	// NWP, NCS and RDY are not requred
	for (int i = 0; i < NAND_DATA_WIDTH; i++) {
		get_gpio(dev, "data-gpios", i, gpios->data[i], GPIOD_IS_IN, err_pin);
	}
#undef get_gpio

	gpio_request_by_name(dev, "nwp-gpios", 0, &gpios->nwp, GPIOD_IS_OUT);
	gpio_request_by_name(dev, "nce-gpios", 0, &gpios->nce, GPIOD_IS_OUT);
	gpio_request_by_name(dev, "rdy-gpios", 0, &gpios->rdy, GPIOD_IS_IN);

	return 0;
}

static int
bb_nand_probe(struct udevice *dev)
{
	int chip_count = 0;
	if ((!dev_read_enabled(dev)) || ((chip_count = dev_get_child_count(dev)) < 1))
		return 1;

	struct gpio_fdt_info* fdt_info = calloc(chip_count, sizeof(struct gpio_fdt_info));

	int i = 0;
	ofnode node;

	dev_for_each_subnode(node, dev) {
		struct gpio_fdt_info* info = fdt_info + i++;
		memset(info, 0, sizeof(struct gpio_fdt_info));
		if (!ofnode_is_enabled(node) || dts_decode_chip(node, info)) {
			chip_count--;
			continue;
		}
	}

	if (chip_count < 1)
		goto err_decode;



	// printf("children: %d\n", child_num);

	free(fdt_info);

	return 1;

err_decode:
	free(fdt_info);
	return 1;

	// puts("here\n");
	// struct bb_nand_info *bb_nand = dev_get_priv(dev);
	// struct nand_chip	 *nand    = {0};
	// struct mtd_info	*our_mtd;

	// puts("1\n");

	// if (bitbang_gpio_init(dev, &bb_nand->gpios)) {
	// 	printf("Could not decode nand-flash in device tree\n");
	// 	return 1;
	// }

	// puts("2\n");
	// nand->cmd_ctrl	 = nand_hwcontrol;
	// nand->read_byte	 = hw_read_byte;
	// nand->write_byte = hw_write_byte;
	// nand->chip_delay = 40;
	// nand->ecc.mode	 = NAND_ECC_SOFT;
	// // nand->dev_ready		 = nand_dev_ready;
	// // nand->select_chip = nand_select_chip;

	// /* Disable subpage writes as we do not provide ecc->hwctl */
	// nand->options |= NAND_NO_SUBPAGE_WRITE | NAND_SKIP_BBTSCAN;

	// /* Configure flash type as P-NAND */
	// // clrsetbits_le32(&info->reg->flash_type, FLASH_PIN, FLASH_TYPE_4K | FLASH_SIZE_436OOB);
	// // config->width = NAND_DATA_WIDTH;

	// puts("3\n");
	// our_mtd = nand_to_mtd(nand);
	// if (nand_scan_ident(our_mtd, CONFIG_SYS_NAND_MAX_CHIPS, NULL))
	// 	goto err;

	// puts("4\n");
	// nand->ecc.size	= BCH_DATA_UNIT;
	// nand->ecc.bytes = BCH_GF_PARAM_M * (nand->ecc.strength / 8);

	// Reconfig flash type according to ONFI
	// nand_config_flash_type(nand);

	// ret = set_bus_width_page_size(our_mtd);
	// if (ret)
	// 	return ret;

	// /* Set the bad block position */
	// // nand->badblockpos = our_mtd->writesize > 512 ? NAND_LARGE_BADBLOCK_POS : NAND_SMALL_BADBLOCK_POS;

	// /* Arrange OOB layout */
	// // ret = nand_config_oob_layout(nand);
	// // if (ret)
	// // 	return ret;

	// /* Init DMA descriptor ring */
	// // ret = init_nand_dma(nand);
	// // if (ret)
	// // 	return ret;

	// if (nand_scan_tail(our_mtd))
	// 	goto err;

	// if (nand_register(0, our_mtd)) {
	// 	pr_err("Failed to register MTD\n");
	// 	goto err;
	// }

	// // ret = set_bus_width_page_size(our_mtd);
	// // if (ret)
	// // 	return ret;

	// printf("NAND      : %s\n", our_mtd->name);
	// printf("Chip  Size: %lldMB\n", nand->chipsize / (1024 * 1024));
	// printf("Block Size: %dKB\n", our_mtd->erasesize / 1024);
	// printf("Page  Size: %dB\n", our_mtd->writesize);
	// printf("OOB   Size: %dB\n", our_mtd->oobsize);

	// return 1;

// 	if (bitbang_gpio_init() || Init_Driver() != DRIVER_STATUS_INITIALIZED) {
// 		printf("gpio init fail \n");
// 	} else {
// 		printf("gpio init success \n");
// 	}
// 	flash_width d[64] = {0};
// #define printbytes(title, data, num) \
// 	do {                                                                                                           \
// 		printf("%s: ", title);                                                                                 \
// 		for (int i = 0; i < num; i++) {                                                                        \
// 			printf("%x", (unsigned int) data[i]);                                                          \
// 		}                                                                                                      \
// 		printf("\n");                                                                                          \
// 	} while (0)

// 	param_page_t pp = {0};

// 	printf("success: %d\n", NAND_Read_ID(d));
// 	printbytes("ID", d, NUM_OF_READID_BYTES);
// 	printf("success: %d\n", NAND_Read_ID_ONFI(d));
// 	printbytes("ID ONFI", d, NUM_OF_READIDONFI_BYTES);
// 	printf("success: %d\n", NAND_Read_Unique_Id(d));
// 	// printbytes("UID", d, NUM_OF_UNIQUEID_BYTES);
// 	printf("success: %d\n", NAND_Read_Param_Page(&pp));
// 	// printf("CMD: %x\n", (unsigned int)pp.command);
// 	// printf("PPB: %x\n", (unsigned int)pp.pages_per_block);
// 	// printf("PPB: %x\n", (unsigned int)pp.blocks_per_lun);
// 	// printf("status: %x\n", NAND_Read_Status());

// 	return 1;
// err:
// 	bitbang_gpio_free(dev, &bb_nand->gpios);
// 	return 1;
}

/*
 * @name: Device name
 * @id: Identifies the uclass we belong to
 * @of_match: List of compatible strings to match, and any identifying data
 * for each.
 * @bind: Called to bind a device to its driver
 * @probe: Called to probe a device, i.e. activate it
 * @remove: Called to remove a device, i.e. de-activate it
 * @unbind: Called to unbind a device from its driver
 * @of_to_plat: Called before probe to decode device tree data
 * @child_post_bind: Called after a new child has been bound
 * @child_pre_probe: Called before a child device is probed. The device has
 * memory allocated but it has not yet been probed.
 * @child_post_remove: Called after a child device is removed. The device
 * has memory allocated but its device_remove() method has been called.
 * @priv_auto: If non-zero this is the size of the private data
 * to be allocated in the device's ->priv pointer. If zero, then the driver
 * is responsible for allocating any data required.
 * @plat_auto: If non-zero this is the size of the
 * platform data to be allocated in the device's ->plat pointer.
 * This is typically only useful for device-tree-aware drivers (those with
 * an of_match), since drivers which use plat will have the data
 * provided in the U_BOOT_DRVINFO() instantiation.
 * @per_child_auto: Each device can hold private data owned by
 * its parent. If required this will be automatically allocated if this
 * value is non-zero.
 * @per_child_plat_auto: A bus likes to store information about
 * its children. If non-zero this is the size of this data, to be allocated
 * in the child's parent_plat pointer.
 * @ops: Driver-specific operations. This is typically a list of function
 * pointers defined by the driver, to implement driver functions required by
 * the uclass.
 * @flags: driver flags - see `DM_FLAGS_...`
 * @acpi_ops: Advanced Configuration and Power Interface (ACPI) operations,
 * allowing the device to add things to the ACPI tables passed to Linux
 */

static const struct udevice_id bb_nand_dt_ids[] = {{.compatible = "onfi,bitbang-nand"}, {/* sentinel */}};

U_BOOT_DRIVER(bitbang_nand) = {
    .name      = "bitbang-nand",
    .id	       = UCLASS_MTD,
    .of_match  = bb_nand_dt_ids,
    .probe     = bb_nand_probe,
    .priv_auto = sizeof(struct bb_nand_info),
};

void
board_nand_init(void)
{
	struct udevice *dev = 0;
	int		ret = 0;

	dm_dump_all();
	ret = uclass_get_device_by_driver(UCLASS_MTD, DM_DRIVER_GET(bitbang_nand), &dev);
	puts("hi\n");

	return;

	if (ret && ret != -ENODEV)
		pr_err("Failed to initialize %s. (error %d)\n", dev->name, ret);
}
